# Yoshino Internationalization
[![Discord](https://discordapp.com/api/guilds/389140727243735053/embed.png)](https://yoshino.gigadrivegroup.com/community)
[![Weblate](http://translate.gigadrivegroup.com/weblate/widgets/yoshino/-/yoshinobot/svg-badge.svg)](https://translate.gigadrivegroup.com/weblate/projects/yoshino/)

## Contribute
Please visit our [Weblate page](https://translate.gigadrivegroup.com/weblate/projects/yoshino/) to contribute. To become a proofreader, please contact a staff member on our [Discord server](https://yoshino.gigadrivegroup.com/community).